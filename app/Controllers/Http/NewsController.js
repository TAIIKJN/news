'use strict'

const News = use('App/Models/News')
const Review = use('App/Models/Review')
const Helpers = use('Helpers')
const Env = use('Env')

class NewsController {
  //News
  async index({request, response}) {
    return await News.all()
  }

  async show({response, params}) {
    const news_id = params.id
    try {
      const news = await News.find(news_id)
      const review = await Review.query()
        .where('news_id', news.id)
        .fetch()
      news.imageFull = Env.get('APP_URL') + `/` + `uploads/` + news.image_news
      if (!news) {
        return response.status(400).json({
          status: "ERROR",
          message: "NOT FOUND USER"
        })
      }
      if (!review) {
        return response.status(400).json({
          status: "ERROR",
          message: "NOT FOUND USER"
        })
      }
      return response.status(200).json({
        news,
        review
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }
  }

  async store({request, response}) {
    const body = request.all();
    try {
      const news = new News()
      news.caption = body.caption
      news.discourse = body.discourse

      //uploadnews
      const image_news = request.file('image_news', {
        types: ['image'],
        size: '2mb'
      })

      news.image_news = `${new Date().getTime()}.${image_news.subtype}`
      await image_news.move(Helpers.publicPath('uploads'), {
        name: news.image_news,
        overwrite: false
      })
      if (!image_news.moved()) {
        return image_news.error();
      }
      await news.save()


      return response.status(201).json({
        status: "success"
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }

  }

  async update({request, response, params}) {
    const body = request.all();
    const news_id = params.id
    try {
      const news = await News.find(news_id)
      news.caption = body.caption
      news.discourse = body.discourse

      // updateimage
      const image_news = request.file('image_news', {
        types: ['image'],
        size: '2mb'
      })
      news.image_news = `${new Date().getTime()}.${image_news.subtype}`
      await image_news.move(Helpers.publicPath('uploads'), {
        name: news.image_news,
        overwrite: false
      })
      if (!image_news.moved()) {
        return image_news.error();
      }

      await news.save()
      return response.status(202).json({
        status: "success"
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }

  }

  async destroy({response, params}) {
    const news_id = params.id
    try {
      const news = await News.find(news_id)
      await news.delete()
      const review = await Review
        .query()
        .where('news_id', news.id)
        .delete()
      console.log(review)
      return response.status(200).json({
        status: "success"
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }
  }

//  Reviwe
  async createreviwe({request, response, params}) {
    const body = request.all();
    const news_id = params.id
    const news = await News.find(news_id)
    try {
      const reviwe = new Review()
      reviwe.news_id = news.id
      reviwe.detail = body.detail
      reviwe.score = body.score
      await reviwe.save()

      return response.status(201).json({
        status: "success"
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }
  }
}

module.exports = NewsController
