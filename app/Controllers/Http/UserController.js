'use strict'

const User = use('App/Models/User')

class UserController {

  async index({request, response}) {
    return await User.all()
  }

  async store({request, response}) {
    const body = request.all();
    try {
      const user = new User()
      if (body.NAME){
        user.name = body.NAME
      }

      user.lastname = body.LASTNAME
      user.email = body.EMAIL
      user.age = body.AGE
      user.sex = body.SEX
      await user.save()

      return response.status(201).json({
        status: "success"
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }
  }
  async show({response, params}) {
    const user_id = params.id
    const user = await User.find(user_id)
    if (!user) {
      return response.status(400).json({
        status: "ERROR",
        message: "NOT FOUND USER"
      })
    }

    return response.status(200).json(user)
  }


  async update({request, response, params}) {
    const body = request.all();
    const user_id = params.id
    try {
      const user = await User.find(user_id)
      user.name = body.NAME
      user.lastname = body.LASTNAME
      user.email = body.EMAIL
      user.age = body.AGE
      user.sex = body.SEX
      await user.save()
      return response.status(202).json({
        status: "success"
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }

  }

  async destroy({response, params}) {
    const user_id = params.id
    try {

      const user = await User.find(user_id)
      await user.delete()

      return response.status(200).json({
        status: "success"
      })
    } catch (e) {
      return response.status(500).json({
        status: "error",
        error: e
      })
    }
  }
}


module.exports = UserController












