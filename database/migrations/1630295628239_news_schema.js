'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NewsSchema extends Schema {
  up () {
    this.table('news', (table) => {
      // alter table
    })
  }

  down () {
    this.table('news', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NewsSchema
