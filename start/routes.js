'use strict'

const Route = use('Route')

Route.on('/').render('welcome')

//News
Route.get('news','NewsController.index').as('news.index')
Route.post('news', 'NewsController.store').as('news.store')
Route.get('news/:id', 'NewsController.show').as('news.show')
Route.patch('news/:id', 'NewsController.update')
Route.delete('news/:id', 'NewsController.destroy').as('news.destroy')

//Reviwe
Route.post('news/:id', 'NewsController.createreviwe').as('news.createreviwe')
